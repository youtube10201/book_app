# book_app

A new Flutter project.
AutoRouter, GetIt and Assets generation example app

## Getting Started

For first start use this

```BASH 
flutter clean
flutter pub get
dart run build_runner build --delete-conflicting-outputs
```

use this command for generating auto router and assets path

```BASH 
dart run build_runner build --delete-conflicting-outputs
```

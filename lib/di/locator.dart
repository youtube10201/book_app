import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../presentation/view/chapter/bloc/chapter_bloc.dart';

final getIt = GetIt.instance;

init() async {
  final sharedPreferences = await SharedPreferences.getInstance();
  getIt.registerLazySingleton<SharedPreferences>(() => sharedPreferences);

  getIt.registerFactory(() => ChapterBloc());
}

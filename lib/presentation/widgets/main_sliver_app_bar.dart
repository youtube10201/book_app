import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../gen/assets.gen.dart';

class MainSliverAppBar extends SliverPersistentHeaderDelegate {
  final double expandedHeight;

  MainSliverAppBar({required this.expandedHeight});

  ValueNotifier<bool> isCollapse = ValueNotifier(false);

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Stack(
      clipBehavior: Clip.none,
      fit: StackFit.expand,
      children: [
        Image.asset(
          Assets.img.backgroundSliver.path,
          fit: BoxFit.cover,
        ),
        Align(
          alignment: Alignment.topCenter,
          child: Row(
            children: [
              IconButton(
                icon: SvgPicture.asset(
                  Assets.icons.homeSmile,
                ),
                onPressed: () {},
              ),
              const Expanded(child: SizedBox()),
              Visibility(
                visible: !isCollapse.value,
                child: IconButton(
                  icon: SvgPicture.asset(Assets.icons.player),
                  onPressed: () {},
                ),
              ),
              IconButton(
                icon: SvgPicture.asset(
                  Assets.icons.list,
                ),
                onPressed: () {},
              ),
              IconButton(
                icon: SvgPicture.asset(
                  Assets.icons.settings,
                ),
                onPressed: () {},
              ),
              IconButton(
                icon: SvgPicture.asset(
                  Assets.icons.share,
                ),
                onPressed: () {},
              ),
            ],
          ),
        ),
        Visibility(
          visible: isCollapse.value,
          child: Align(
            alignment: Alignment.bottomRight,
            child: IconButton(
              icon: SvgPicture.asset(Assets.icons.bouncingPlayer),
              onPressed: () {},
            ),
          ),
        ),
      ],
    );
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => kToolbarHeight;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}

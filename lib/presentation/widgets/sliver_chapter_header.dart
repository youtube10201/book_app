import 'package:flutter/material.dart';

import '../../core/utils/theme.dart';

class ChapterHeaderSliver extends SliverPersistentHeaderDelegate {
  final VoidCallback back;
  final VoidCallback forward;

  final ValueNotifier<double> readProgress;

  ChapterHeaderSliver({required this.readProgress, required this.back, required this.forward});

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return DecoratedBox(
      decoration: BoxDecoration(color: kLightTheme.colorScheme.background),
      child: Column(
        children: [
          LinearProgressIndicator(
            color: kLightTheme.colorScheme.primary,
            value: readProgress.value < 0.0 ? 0.0 : readProgress.value,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                onPressed: () {
                  back();
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: kLightTheme.colorScheme.primary,
                ),
              ),
              const Text('Chapter '),
              IconButton(
                onPressed: () {
                  forward();
                },
                icon: Icon(
                  Icons.arrow_forward_ios,
                  color: kLightTheme.colorScheme.primary,
                ),
              ),
            ],
          ),
          const Divider(
            height: 1,
            color: Colors.grey,
          )
        ],
      ),
    );
  }

  @override
  double get maxExtent => 56;

  @override
  double get minExtent => kToolbarHeight;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}

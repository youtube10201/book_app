import 'package:auto_route/auto_route.dart';
import 'package:book_app/core/utils/theme.dart';
import 'package:book_app/di/locator.dart';
import 'package:book_app/presentation/view/chapter/bloc/chapter_bloc.dart';
import 'package:book_app/presentation/view/chapter/widget/opinion_block.dart';
import 'package:book_app/presentation/widgets/main_sliver_app_bar.dart';
import 'package:book_app/presentation/widgets/sliver_chapter_header.dart';
import 'package:flutter/material.dart';

@RoutePage()
class ChapterScreen extends StatefulWidget {
  const ChapterScreen({super.key});

  @override
  State<ChapterScreen> createState() => _ChapterScreenState();
}

class _ChapterScreenState extends State<ChapterScreen> {
  late final PageController _pageViewController;
  late final ScrollController _scrollController;
  late final ValueNotifier<double> _readProgress;
  late final ChapterBloc _chapterBloc;

  @override
  void initState() {
    _pageViewController = PageController();
    _scrollController = ScrollController();
    _readProgress = ValueNotifier(0.0);
    _scrollController.addListener(() {
      _readProgress.value =
          (_scrollController.position.pixels / (_scrollController.position.maxScrollExtent / 100)) / 100;
    });
    _chapterBloc = getIt.get<ChapterBloc>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: NestedScrollView(
          controller: _scrollController,
          physics: const BouncingScrollPhysics(),
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverPersistentHeader(
                pinned: true,
                delegate: MainSliverAppBar(
                  expandedHeight: 275,
                ),
              ),
              SliverPersistentHeader(
                  pinned: true,
                  floating: false,
                  delegate: ChapterHeaderSliver(
                    back: () {
                      _pageViewController.previousPage(
                          duration: const Duration(milliseconds: 500), curve: Curves.easeIn);
                    },
                    forward: () {
                      _pageViewController.nextPage(duration: const Duration(milliseconds: 500), curve: Curves.easeIn);
                    },
                    readProgress: _readProgress,
                  )),
            ];
          },
          body: PageView.builder(
              physics: const NeverScrollableScrollPhysics(),
              controller: _pageViewController,
              itemBuilder: (_, index) {
                return ListView(
                  physics: const NeverScrollableScrollPhysics(),
                  children: [
                    Text(
                      'Приключения Принца Антара',
                      style: kLightTheme.textTheme.titleLarge,
                    ),
                    Text(
                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum auctor diam non orci elementum, ut malesuada mauris maximus. Donec aliquet pulvinar justo vitae tempus. Curabitur scelerisque ligula non libero hendrerit interdum. Vivamus blandit fringilla nibh. In blandit rutrum ultricies. Maecenas fermentum efficitur porta. Donec imperdiet ut libero vitae accumsan. Aenean auctor nunc vel sapien dapibus, eu molestie eros tincidunt. Vestibulum sem ligula, iaculis at velit eu, maximus malesuada nibh. Donec auctor eget sapien luctus pretium. Mauris scelerisque hendrerit ultricies. Nunc convallis orci mattis rutrum ultricies. Proin et nunc id libero pulvinar tempor hendrerit eget metus.\nMauris efficitur tortor convallis libero viverra, nec pellentesque ligula condimentum. In vel ante a mi aliquam convallis. Aliquam ac fringilla nunc. Donec ac tellus nec nibh pharetra bibendum sit amet non eros. In placerat consequat mollis. Sed pulvinar ultricies tempor. Nulla malesuada purus in purus ornare placerat. Donec eget imperdiet dolor. Nulla rutrum et leo et malesuada. Quisque hendrerit ultricies lacus, sodales eleifend nulla malesuada quis. Phasellus non erat ac enim pulvinar maximus. Cras pulvinar dictum elit. Vivamus tortor purus, consequat non interdum vitae, condimentum id nisl.\nPraesent rutrum, lectus quis tempus dapibus, tellus nulla facilisis neque, vel aliquam elit elit id elit. Etiam molestie ut odio ut eleifend. Aenean ut eros ante. Nulla id est eu nisi placerat egestas. Vivamus a massa condimentum, egestas ante ac, molestie magna. Cras non purus venenatis massa eleifend pulvinar sed eget leo. Integer dapibus, nunc sed interdum tempus, turpis nisi pretium felis, in iaculis odio mi quis ligula. Sed blandit augue vitae nunc porta sagittis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nec nunc vel tortor vehicula bibendum.\nProin lectus lectus, tempor ac ante quis, vehicula scelerisque dui. Fusce non magna malesuada, convallis lacus at, euismod tellus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean quis mattis erat. Suspendisse lobortis id ante eget finibus. Quisque ipsum orci, malesuada nec urna vitae, mattis posuere massa. Fusce porta diam varius ex finibus efficitur a ac felis. Aliquam condimentum faucibus nisl sed pulvinar. Quisque tempus tortor ullamcorper fermentum pharetra. Mauris eget libero finibus, ultrices nibh et, euismod est.\nAliquam sit amet tempus metus, at porta erat. Pellentesque eget arcu a turpis sollicitudin pretium ac vel arcu. Pellentesque egestas turpis quis efficitur lacinia. Vivamus condimentum sagittis libero, vitae laoreet tortor imperdiet nec. Ut bibendum, ante a convallis condimentum, libero massa aliquet nisl, nec mattis libero eros non nunc. Nullam ex odio, ornare fermentum nulla posuere, laoreet lacinia nisl. Morbi ipsum orci, bibendum pharetra ligula ac, mattis posuere enim. Integer pellentesque nec elit quis euismod. Integer elementum metus eu velit tristique aliquet.\nGenerated 5 paragraphs, 439 words, 3025 bytes of Lorem Ipsum",
                      style: kLightTheme.textTheme.titleSmall,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const OpinionBlock(listOptions: [
                      'Принц пойдет налево в башню Саурона',
                      'Принц пойдет направо в царство Кощея Бессмертного',
                      'Принц останется дома и будет пить пиво',
                    ]),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                );
              }),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _pageViewController.dispose();
    _scrollController.dispose();
    _readProgress.dispose();
    super.dispose();
  }
}

import 'package:book_app/core/utils/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../gen/assets.gen.dart';
import 'custom_radio_list_tile.dart';

class OpinionBlock extends StatefulWidget {
  final List<String> listOptions;

  const OpinionBlock({super.key, required this.listOptions});

  @override
  State<OpinionBlock> createState() => _OpinionBlockState();
}

class _OpinionBlockState extends State<OpinionBlock> {
  @override
  Widget build(BuildContext context) {
    String currentOption = widget.listOptions.first;
    return DecoratedBox(
      decoration: BoxDecoration(
        gradient: const LinearGradient(
          colors: [
            Color(0xffFF8966),
            Color(0xffFFBB4B),
          ],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
        borderRadius: BorderRadius.circular(24),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: SvgPicture.asset(Assets.icons.optionTitleIcon),
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.85,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Как дальше же поступит наш принц?',
                        maxLines: 2,
                        style: kLightTheme.textTheme.titleMedium?.copyWith(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          ListView.builder(
              shrinkWrap: true,
              itemCount: widget.listOptions.length,
              itemBuilder: (_, index) {
                return CustomRadioListTile(
                  value: widget.listOptions[index],
                  onChanged: (value) {
                    setState(() {
                      currentOption = value.toString();
                    });
                  },
                  groupValue: currentOption,
                  text: widget.listOptions[index],
                  withLeftIcon: false,
                );
              }),
          CustomRadioListTile(
            value: 'Напишу свой вариант',
            onChanged: (value) {
              setState(() {
                currentOption = value.toString();
              });
            },
            groupValue: currentOption,
            text: 'Напишу свой вариант',
            withLeftIcon: true,
          ),
          Padding(
            padding: const EdgeInsets.all(12),
            child: SizedBox(
              width: double.infinity,
              height: 56,
              child: ElevatedButton(
                style: kLightTheme.elevatedButtonTheme.style,
                onPressed: () {},
                child: Text(
                  'Continue',
                  style: kLightTheme.textTheme.titleMedium?.copyWith(color: Colors.white),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

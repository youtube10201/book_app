import 'package:book_app/core/utils/theme.dart';
import 'package:book_app/gen/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CustomRadioListTile<T> extends StatelessWidget {
  final T value;
  final T? groupValue;

  final String text;
  final ValueChanged<T?> onChanged;
  final bool withLeftIcon;

  const CustomRadioListTile({
    super.key,
    required this.value,
    required this.groupValue,
    required this.text,
    required this.onChanged,
    required this.withLeftIcon,
  });

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(minHeight: 56),
      child: Container(
        decoration: BoxDecoration(
          color: kLightTheme.colorScheme.background,
          borderRadius: BorderRadius.circular(16),
        ),
        margin: const EdgeInsets.all(8),
        child: InkWell(
          onTap: () => onChanged(value),
          child: Padding(
            padding: const EdgeInsets.all(5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Visibility(
                  visible: withLeftIcon,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SvgPicture.asset(Assets.icons.edit),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: Text(
                    text,
                    maxLines: 3,
                    style: kLightTheme.textTheme.titleMedium?.copyWith(color: Colors.black),
                  ),
                ),
                const SizedBox(width: 5),
                Container(
                    width: 50,
                    height: 50,
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    decoration: ShapeDecoration(
                      shape: const CircleBorder(
                        side: BorderSide(
                          color: Colors.transparent,
                        ),
                      ),
                      color: value == groupValue ? const Color(0xffFFBB4B) : kLightTheme.colorScheme.secondary,
                    ),
                    child: Visibility(
                      visible: value == groupValue,
                      child: SizedBox(
                        height: 50,
                        width: 50,
                        child: SvgPicture.asset(Assets.icons.checkRead),
                      ),
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

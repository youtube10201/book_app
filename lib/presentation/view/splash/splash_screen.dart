import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:auto_route/auto_route.dart';
import 'package:book_app/app_router.gr.dart';
import 'package:book_app/core/utils/theme.dart';
import 'package:book_app/gen/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../../core/utils/constants.dart';

@RoutePage()
class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Future.delayed(
      const Duration(seconds: 3),
      () {
        AutoRouter.of(context).replace(const ChapterRoute());
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kLightTheme.colorScheme.background,
      body: SafeArea(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.only(top: 100.0),
            child: Column(children: [
              Lottie.asset(Assets.anim.bookAnimation),
              DefaultTextStyle(
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: kLightTheme.textTheme.titleSmall?.color,
                  fontSize: 30.0,
                  fontFamily: 'Bobbers',
                ),
                child: AnimatedTextKit(animatedTexts: [
                  TyperAnimatedText(
                    kAppTitle,
                    speed: const Duration(milliseconds: 75),
                  ),
                ]),
              ),
            ]),
          ),
        ),
      ),
    );
    ;
  }
}

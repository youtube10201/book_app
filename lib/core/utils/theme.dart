import 'package:flutter/material.dart';

final ThemeData kLightTheme = ThemeData(
  brightness: Brightness.light,
  colorScheme: const ColorScheme.light(
    primary: Color(0xFFFF8966),
    secondary: Color(0xffE5EBED),
    background: Colors.white,
  ),
  textTheme: const TextTheme(
    titleLarge: TextStyle(color: Color(0xFF000000), fontSize: 32),
    titleMedium: TextStyle(color: Color(0xFF6D7885), fontSize: 24),
    titleSmall: TextStyle(color: Color(0xFF000000), fontSize: 16),
  ),
  iconTheme: const IconThemeData(
    color: Color(0xFFFF8966),
  ),
  cardTheme: const CardTheme(
    color: Color.fromRGBO(255, 255, 255, 0.1),
    shadowColor: Color(0xFF453e46),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24.0),
        ),
      ),
      backgroundColor: const MaterialStatePropertyAll(
        Color(0xff18152D),
      ),
      textStyle: const MaterialStatePropertyAll(
        TextStyle(color: Colors.white),
      ),
    ),
  ),
);
